from pprint import pprint

def solve(n):
        col = set()
        pos_diag = set() # (row + col)
        neg_diag = set() # (row - col)
        print(pos_diag, neg_diag)

        result = []
        board = [["."] * n for i in range(n)]

        def backtrack(r):
            if r == n:
                copy = ["".join(row) for row in board]
                result.append(copy)
                return

            for c in range(n):
                if c in col or (r + c) in pos_diag or (r - c) in neg_diag:
                    continue 


                col.add(c)
                pos_diag.add(r + c)
                neg_diag.add(r - c)
                board[r][c] = "Q"

                backtrack(r + 1)

                col.remove(c)
                pos_diag.remove(r + c)
                neg_diag.remove(r - c)
                board[r][c] = "."

        backtrack(0)
        return result

pprint(solve(4))







# def is_safe(board, pos_row, pos_column):
#     row = board[pos_row]

#     # check safety of row
#     for value in row:
#         if value != 0:
#             return False

#     # check safety of column
#     for row2 in board:
#         if row2[pos_column] != 0:
#             return False
            
#     return True

# def place_rook_in_column(board, column_idx):
#     if column_idx >= 4:
#         return True

#     for row_idx in range(0,4):
#         if is_safe(board, row_idx, column_idx):
#             board[row_idx][column_idx] = 1
#             if place_rook_in_column(board, column_idx+1):
#                 return True
#             else:
#                 board[row_idx][column_idx] = 0

#     return False  # NO solution possible


# def four_rooks():
#     board = [
#         [0, 0, 0, 0],
#         [0, 0, 0, 0],
#         [0, 0, 0, 0],
#         [0, 0, 0, 0],
#     ]

#     place_rook_in_column(board, 0)
#     return board # solved board


# if __name__ == "__main__":
#     board = four_rooks()
#     pprint(board, width=20)